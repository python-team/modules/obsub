obsub (0.2.1-2) unstable; urgency=medium

  * Team upload.
  * Replace Nose with Pytest (Closes: #1018427)

 -- Alexandre Detiste <tchet@debian.org>  Wed, 07 Aug 2024 10:47:17 +0200

obsub (0.2.1-1) unstable; urgency=medium

  * Team upload.
  * New upstream version 0.2.1
  * Update upstream URL
  * Use new dh-sequence-python3
  * Set "Rules-Requires-Root: no"
  * d/watch: Set version=4

 -- Alexandre Detiste <tchet@debian.org>  Sat, 11 May 2024 17:21:56 +0200

obsub (0.2-5) unstable; urgency=low

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Database, Bug-Submit.

  [ Ondřej Nový ]
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Debian Janitor ]
  * Bump debhelper from old 12 to 13.

 -- Sandro Tosi <morph@debian.org>  Wed, 04 May 2022 16:22:10 -0400

obsub (0.2-4) unstable; urgency=medium

  * Team upload.
  * Fixed short description (Closes: #860462)
  * d/control
    - Use https in Vcs-Git
    - Add autopkgtest-pkg-python testsuite
  * Bumped debhelper compat version to 12
  * d/control: Set Vcs-* to salsa.debian.org
  * d/copyright: Use https protocol in Format field
  * d/watch: Use https protocol
  * Rename d/tests/control.autodep8 to d/tests/control.
  * Use debhelper-compat instead of debian/compat.
  * Drop Python 2 support.

 -- Ondřej Nový <onovy@debian.org>  Sat, 03 Aug 2019 22:08:09 +0200

obsub (0.2-3) unstable; urgency=medium

  * Fix typo in debian/watch

 -- Free Ekanayaka <freee@debian.org>  Mon, 12 Dec 2016 14:20:54 +0000

obsub (0.2-2) unstable; urgency=medium

  * Add setup-read-bytes patch to read README.rst as bytes and
    then convert using UTF-8 encoding (Closes: #844934).

 -- Free Ekanayaka <freee@debian.org>  Wed, 23 Nov 2016 04:43:58 +0000

obsub (0.2-1) unstable; urgency=medium

  * Initial release (Closes: #844436)

 -- Free Ekanayaka <freee@debian.org>  Tue, 15 Nov 2016 21:17:59 +0000
